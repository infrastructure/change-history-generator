from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from email.utils import parsedate_to_datetime
from pathlib import Path
from typing import TYPE_CHECKING, Any, Iterable, Iterator, Optional

import argparse
import contextlib
import dataclasses
import gzip
import json
import logging
import os
import tarfile

from debian import deb822
from debian.changelog import Changelog
from debian.debian_support import Version
from dulwich.errors import NotGitRepository
from dulwich.graph import find_merge_base
from dulwich.mailmap import Mailmap
from dulwich.repo import Commit, Repo
from dulwich.walk import Walker

if TYPE_CHECKING:
    from dulwich.walk import WalkEntry  # noqa: F401


@dataclass
class BuildMetadata:
    filename: str
    recipes_url: Optional[str] = None
    recipes_commit: Optional[str] = None

    @staticmethod
    def parse_env(path: Path) -> 'BuildMetadata':
        meta = BuildMetadata(filename=path.name)

        with path.open() as fp:
            for line in fp:
                line = line.strip()

                try:
                    k, v = line.split('=', 1)
                except ValueError:
                    continue

                match k:
                    case 'RECIPES_URL':
                        meta.recipes_url = v
                    case 'RECIPES_COMMIT':
                        meta.recipes_commit = v

        return meta


@dataclass(frozen=True)
class Image:
    name: str

    @staticmethod
    def from_filename(
        filename: str,
        *,
        version: str,
    ) -> Optional['Image']:
        prefix = next(
            filter(
                filename.startswith,
                ('apertis_ostree_', 'apertis_', 'isolation_rfs-', 'ospack_'),
            ),
            '',
        )
        if not prefix:
            return None

        filename = filename.removeprefix(prefix)

        # Strip off the post-prefix release.
        if '-' not in filename:
            return None
        filename = filename.split('-', 1)[1]

        extension = next(filter(filename.endswith, ('.img', '.commit')), '')
        if extension:
            filename = filename.removesuffix(extension)

        for separator in '_-':
            version_suffix = separator + version
            if filename.endswith(version_suffix):
                break
        else:
            return None

        filename = filename.removesuffix(version_suffix)

        return Image(name=f'{prefix}-{filename}{extension}')


@dataclass
class PackageInfo:
    version: Version
    source: str
    source_version: Version


class ChangelogArchive:
    def __init__(self, tar: tarfile.TarFile) -> None:
        self.tar = tar

    def get(self, source: str) -> Optional[Changelog]:
        try:
            fp = self.tar.extractfile(f'{source}.changelog')
        except KeyError:
            return None

        return Changelog(fp)


@dataclass
class ImageArtifacts:
    root: 'ImagesRoot'
    path: Path

    @property
    def versioned_relative_path(self) -> Path:
        return self.root.version / self.path.relative_to(self.root.path)

    def list_packages(self) -> dict[str, PackageInfo]:
        pkglist = self.path.with_name(self.path.name + '.pkglist.gz')
        with gzip.open(pkglist) as gz:
            return {
                package['package']: PackageInfo(
                    version=package.get_version(),
                    source=package.source or package['package'],
                    source_version=package.source_version,
                )
                for package in deb822.Packages.iter_paragraphs(gz)
            }

    @contextlib.contextmanager
    def open_changelogs(self) -> Iterator[ChangelogArchive]:
        changelogs = self.path.with_name(self.path.name + '.changelogs.tar.gz')
        with tarfile.open(changelogs) as tar:
            yield ChangelogArchive(tar)


class ImagesRoot:
    def __init__(self, path: os.PathLike[str] | str) -> None:
        self.path = Path(path)

    @property
    def version(self) -> str:
        return self.path.name

    def list_images(self) -> Iterable[tuple[Image, ImageArtifacts]]:
        version = self.version

        for arch in os.listdir(self.path):
            arch_dir = self.path / arch
            for variant in os.listdir(arch_dir):
                variant_dir = arch_dir / variant
                for pkglist in variant_dir.glob('*.pkglist.gz'):
                    image_filename = pkglist.name.removesuffix('.pkglist.gz')
                    image = Image.from_filename(image_filename, version=version)
                    if image is None:
                        logging.warning(f'unsupported pkglist path {pkglist}')
                        continue

                    yield (image, ImageArtifacts(self, variant_dir / image_filename))

    def list_metadata(self) -> Iterable[BuildMetadata]:
        for env_file in (self.path / 'meta').glob('build-env-*.txt'):
            if (meta := BuildMetadata.parse_env(env_file)) is None:
                logging.warning(f'failed to load recipes repo from {env_file}')
                continue

            yield meta


@dataclass(frozen=True)
class RecipesRepo:
    path: Path
    url: str


class RecipesRoot:
    def __init__(self, path: os.PathLike[str] | str) -> None:
        self.path = Path(path)

    def get_repo_for_url(self, url: str) -> Optional[RecipesRepo]:
        name = url.rstrip('/').rsplit('/', 1)[-1]
        if not name:
            return None

        return RecipesRepo(path=self.path / name, url=url)


@dataclass
class SourceChangelogEntry:
    change: str
    author: str


@dataclass
class SourceChangelogBlock:
    author: Optional[str]
    timestamp: Optional[str]
    version: str
    changes: list[SourceChangelogEntry | str]


@dataclass(frozen=True)
class PackageChanges:
    previous: Optional[PackageInfo]
    current: Optional[PackageInfo]


@dataclass(frozen=True)
class ImageChanges:
    previous: Optional[Path]
    current: Optional[Path]
    packages: dict[str, PackageChanges]
    source_changelogs: dict[str, list[SourceChangelogBlock]]


@dataclass(frozen=True)
class RecipesCommit:
    id: str
    author: str
    message: str
    author_timestamp: str
    commit_timestamp: str


@dataclass(frozen=True)
class RecipesChanges:
    url: str
    previous: Optional[str]
    current: Optional[str]
    log: list[RecipesCommit]


@dataclass(frozen=True)
class Changes:
    images: list[ImageChanges]
    recipes: list[RecipesChanges]


def format_source_changelog_block(
    lines: list[str],
    *,
    version_author: Optional[str],
    origin_package: str,
) -> list[SourceChangelogEntry | str]:
    changes: list[SourceChangelogEntry | str] = []
    current_author = None
    new_entry_required = True

    for line in lines:
        line = line.lstrip()
        if not line:
            continue

        if line.startswith('['):
            if not line.endswith(']'):
                logging.warning(
                    f'{origin_package} changelog entry has invalid author block: {line}'
                )
                continue

            current_author = line.lstrip('[ ').rstrip('] ')
            if version_author is not None and (
                current_author == version_author
                or current_author == version_author.split('<')[0].strip()
            ):
                current_author = None

            new_entry_required = True
        elif (asterisk_prefix := line.startswith('*')) or new_entry_required:
            if not asterisk_prefix:
                logging.warning(
                    f'{origin_package} changelog entry starts with invalid line: {line}'
                )

            changes.append(
                SourceChangelogEntry(line.lstrip('* '), current_author)
                if current_author is not None
                else line.lstrip('* ')
            )
            new_entry_required = False
        elif line.startswith('Signed-off-by:'):
            continue
        else:
            if isinstance(changes[-1], SourceChangelogEntry):
                changes[-1].change += '\n' + line
            else:
                changes[-1] += '\n' + line

    return changes


@dataclass
class SourceVersionBounds:
    min_exclusive: Version
    max_inclusive: Version

    @staticmethod
    def from_single_version(version: Version) -> 'SourceVersionBounds':
        return SourceVersionBounds(min_exclusive=version, max_inclusive=version)

    @property
    def empty(self) -> bool:
        return self.min_exclusive == self.max_inclusive

    def merge(self, version: Version) -> None:
        self.min_exclusive = min(self.min_exclusive, version)
        self.max_inclusive = max(self.max_inclusive, version)


def get_changelog_blocks_in_range(
    changelog: Changelog,
    bounds: SourceVersionBounds,
    *,
    origin_package: str,
) -> Iterable[SourceChangelogBlock]:
    for block in changelog:
        if block.version <= bounds.min_exclusive:
            break
        elif block.version > bounds.max_inclusive:
            continue

        yield SourceChangelogBlock(
            author=block.author,
            timestamp=parsedate_to_datetime(block.date).isoformat()
            if block.date is not None
            else None,
            version=str(block.version),
            changes=format_source_changelog_block(
                block.changes(),
                version_author=block.author,
                origin_package=origin_package,
            ),
        )


def find_image_changes(
    previous_artifacts: Optional[ImageArtifacts],
    current_artifacts: Optional[ImageArtifacts],
) -> ImageChanges:
    packages: dict[str, PackageChanges] = {}
    source_changelogs: dict[str, list[SourceChangelogBlock]] = {}

    if current_artifacts is None:
        assert previous_artifacts is not None
        logging.info(
            f'found removed image {previous_artifacts.versioned_relative_path}'
        )
    elif previous_artifacts is None:
        logging.info(f'found new image {current_artifacts.versioned_relative_path}')
    else:
        logging.info(
            f'looking for changes in {previous_artifacts.path}'
            + f' vs {current_artifacts.path}'
        )

    if previous_artifacts is not None and current_artifacts is not None:
        previous_packages = previous_artifacts.list_packages()
        current_packages = current_artifacts.list_packages()

        source_version_bounds: dict[str, SourceVersionBounds] = {}

        for package in set(previous_packages) | set(current_packages):
            previous_info = previous_packages.get(package)
            current_info = current_packages.get(package)

            if previous_info == current_info:
                continue

            packages[package] = PackageChanges(
                previous=previous_info,
                current=current_info,
            )

            for v in previous_info, current_info:
                if v is not None:
                    if v.source in source_version_bounds:
                        source_version_bounds[v.source].merge(v.source_version)
                    else:
                        source_version_bounds[
                            v.source
                        ] = SourceVersionBounds.from_single_version(
                            v.source_version,
                        )

        logging.info(f'found {len(packages)} changed package(s)')

        with contextlib.ExitStack() as stack:
            try:
                changelogs = stack.enter_context(current_artifacts.open_changelogs())
            except FileNotFoundError as ex:
                logging.warning(f'no changelog found at {ex.filename}')
            else:
                for source, bounds in source_version_bounds.items():
                    if bounds.empty:
                        continue

                    changelog = changelogs.get(source)
                    if changelog is None:
                        logging.warning(f'{source} is missing a changelog')
                        continue

                    source_changelogs[source] = list(
                        get_changelog_blocks_in_range(
                            changelog,
                            bounds,
                            origin_package=source,
                        )
                    )

                logging.info(f'saved {len(source_changelogs)} changelog(s)')

    return ImageChanges(
        previous=previous_artifacts.versioned_relative_path
        if previous_artifacts is not None
        else None,
        current=current_artifacts.versioned_relative_path
        if current_artifacts is not None
        else None,
        packages=packages,
        source_changelogs=source_changelogs,
    )


def find_all_image_changes(
    images_root_a: ImagesRoot,
    images_root_b: ImagesRoot,
) -> Iterable[ImageChanges]:
    images_a, images_b = (
        dict(root.list_images()) for root in (images_root_a, images_root_b)
    )

    all_images = set(images_a) | set(images_b)
    logging.info(f'scanning {len(all_images)} image(s)')

    for image in all_images:
        previous_artifacts = images_a.get(image)
        current_artifacts = images_b.get(image)

        yield find_image_changes(
            previous_artifacts,
            current_artifacts,
        )


def list_commits_in_range(
    repo_path: Path,
    *,
    oldest: str,
    newest: str,
) -> Iterable[tuple[Commit, bytes]]:
    try:
        repo = Repo(str(repo_path))
    except NotGitRepository:
        logging.warning(f'repo {repo_path} does not contain a git repository')
        return

    mailmap: Mailmap
    try:
        mailmap = Mailmap.from_path(str(repo_path / '.mailmap'))  # type: ignore
    except FileNotFoundError:
        mailmap = Mailmap()  # type: ignore

    oldest_bytes = oldest.encode('ascii')
    newest_bytes = newest.encode('ascii')
    bases: list[bytes] = find_merge_base(
        repo,
        [oldest_bytes, newest_bytes],
    )  # type: ignore
    if not bases:
        logging.warning(
            f'repo {repo_path} commits {oldest} and {newest} have no common ancestor'
        )
        return

    walker: Walker = repo.get_walker([newest_bytes])
    for entry in walker:  # type: WalkEntry
        commit: Commit = entry.commit
        if commit.id in bases:
            break

        author: bytes = mailmap.lookup(commit.author)  # type: ignore
        yield commit, author


def git_ts_to_iso(*, seconds: int, tz_offset_seconds: int) -> str:
    return datetime.fromtimestamp(
        seconds,
        timezone(timedelta(seconds=tz_offset_seconds)),
    ).isoformat()


def find_recipes_changes(
    recipes: RecipesRepo,
    *,
    previous_commit: Optional[str],
    current_commit: Optional[str],
) -> RecipesChanges:
    log: list[RecipesCommit] = []

    if current_commit is None:
        assert previous_commit is not None
        logging.info(f'found removed recipes {recipes.path} @ {previous_commit}')
    elif previous_commit is None:
        logging.info(f'found new recipes {recipes.path} @ {current_commit}')
    else:
        logging.info(
            f'looking for changes in {recipes.url} from'
            + f' {previous_commit} to {current_commit}'
        )

    if previous_commit is not None and current_commit is not None:
        for commit, author in list_commits_in_range(
            recipes.path,
            oldest=previous_commit,
            newest=current_commit,
        ):
            log.append(
                RecipesCommit(
                    id=commit.id.decode('ascii'),
                    author=author.decode('utf-8'),
                    message=commit.message.decode('utf-8').strip(),
                    # Reformat the seconds since epoch as ISO-8601.
                    author_timestamp=git_ts_to_iso(
                        seconds=commit.author_time,
                        tz_offset_seconds=commit.author_timezone,
                    ),
                    commit_timestamp=git_ts_to_iso(
                        seconds=commit.commit_time,
                        tz_offset_seconds=commit.commit_timezone,
                    ),
                )
            )

        log.reverse()
        logging.info(f'found {len(log)} commit(s) added')

    return RecipesChanges(
        url=recipes.url,
        previous=previous_commit,
        current=current_commit,
        log=log,
    )


def find_all_recipes_changes(
    images_root_a: ImagesRoot,
    images_root_b: ImagesRoot,
    recipes_root: RecipesRoot,
) -> Iterable[RecipesChanges]:
    recipes_commits_a: dict[RecipesRepo, str] = {}
    recipes_commits_b: dict[RecipesRepo, str] = {}

    for root, commits in (
        (images_root_a, recipes_commits_a),
        (images_root_b, recipes_commits_b),
    ):
        for meta in root.list_metadata():
            if meta.recipes_url is None or meta.recipes_commit is None:
                logging.warning(f'{meta.filename} missing recipes url/commit')
                continue

            recipes_repo = recipes_root.get_repo_for_url(meta.recipes_url)
            if recipes_repo is None:
                logging.warning(
                    f'{meta.filename} contains invalid url {meta.recipes_url}'
                )
                continue

            if recipes_repo in commits:
                logging.warning(
                    f'{meta.filename} contains duplicate url {recipes_repo.url}'
                )
                continue

            commits[recipes_repo] = meta.recipes_commit

    all_repos = set(recipes_commits_a) | set(recipes_commits_b)
    logging.info(f'scanning {len(all_repos)} recipes repo(s)')

    for repo in all_repos:
        previous_commit = recipes_commits_a.get(repo)
        current_commit = recipes_commits_b.get(repo)

        yield find_recipes_changes(
            repo,
            previous_commit=previous_commit,
            current_commit=current_commit,
        )


class JSONEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, (Path, Version)):
            return str(o)
        else:
            return super(JSONEncoder, self).default(o)


def dict_factory_exclude_none_keys(items: list[tuple[str, Any]]) -> dict[str, Any]:
    return {k: v for k, v in items if v is not None}


def find_all_changes(
    images_root_a: ImagesRoot,
    images_root_b: ImagesRoot,
    recipes_root: RecipesRoot,
) -> Changes:
    return Changes(
        images=sorted(
            find_all_image_changes(images_root_a, images_root_b),
            key=lambda c: (c.previous or Path(''), c.current or Path('')),
        ),
        recipes=sorted(
            find_all_recipes_changes(images_root_a, images_root_b, recipes_root),
            key=lambda c: c.url,
        ),
    )


def main() -> None:
    parser = argparse.ArgumentParser(
        description='Generate a change history between two images root directories'
        + ' and their corresponding recipes'
    )

    parser.add_argument('--images-root-a', type=ImagesRoot, required=True)
    parser.add_argument('--images-root-b', type=ImagesRoot, required=True)
    parser.add_argument('--recipes-root', type=RecipesRoot, required=True)
    parser.add_argument('--out', type=Path, required=True)

    args = parser.parse_args()

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

    images_root_a: ImagesRoot = args.images_root_a
    images_root_b: ImagesRoot = args.images_root_b
    recipes_root: RecipesRoot = args.recipes_root
    out: Path = args.out

    changes = find_all_changes(images_root_a, images_root_b, recipes_root)

    out_tmp = out.with_name(out.name + '.tmp')
    with out_tmp.open('w') as fp:
        json.dump(
            dataclasses.asdict(
                changes,
                dict_factory=dict_factory_exclude_none_keys,
            ),
            fp,
            cls=JSONEncoder,
            indent=2,
        )

    out_tmp.rename(out)


if __name__ == '__main__':
    main()
