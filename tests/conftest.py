import os.path
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

BRANCHES = ('v2024dev1', 'v2024dev2')
VERSIONS = ('20230101.1212', '20230202.1212')

RECIPES_URL = 'https://gitlab.apertis.org/infrastructure/apertis-image-recipes'
