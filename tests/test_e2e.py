from pathlib import Path

import gzip
import io
import subprocess
import tarfile

from debian.debian_support import Version

from generate import (
    ImageChanges,
    ImagesRoot,
    PackageChanges,
    PackageInfo,
    RecipesChanges,
    RecipesCommit,
    RecipesRoot,
    SourceChangelogBlock,
    find_all_changes,
)

from conftest import BRANCHES, RECIPES_URL, VERSIONS

TEST_PKGLIST_1 = """
Package: p1
Version: 1

Package: p2
Version: 3
Source: s1

Package: p3
Version: 5
Source: s2 (14)

Package: p4
Version: 4
Source: s1

"""

TEST_PKGLIST_2 = """
Package: p1
Version: 2
Source: s2 (12)

Package: p2
Version: 5
Source: s1

Package: p3
Version: 5
Source: s2 (14)

Package: p5
Version: 0
"""


TEST_CHANGELOG_S1 = """
s1 (6) unstable; urgency=medium

  * s1 6

 -- John Doe <john.doe@test.com>  Mon, 02 Oct 2023 07:40:20 +0100

s1 (5) unstable; urgency=medium

  * s1 5

 -- John Doe <john.doe@test.com>  Thu, 31 Aug 2023 06:19:46 +0100

s1 (4) unstable; urgency=medium

  * s1 4

 -- John Doe <john.doe@test.com>  Thu, 09 Jun 2023 19:36:15 +0100

s1 (3) unstable; urgency=medium

  * s1 3

 -- John Doe <john.doe@test.com>  Mon, 16 Feb 2023 06:27:02 +0100
"""


def add_changelog(tar: tarfile.TarFile, source: str, contents: str) -> None:
    contents_bytes = contents.encode('utf-8')
    info = tarfile.TarInfo(name=f'{source}.changelog')
    info.size = len(contents_bytes)
    tar.addfile(info, io.BytesIO(contents_bytes))


def test_e2e(tmp_path: Path) -> None:
    recipes_root = tmp_path / 'recipes'
    recipes = recipes_root / 'apertis-image-recipes'

    def git(*args, **kw):
        return subprocess.run(['git', '-C', str(recipes), *args], check=True, **kw)

    def git_commit(message, *, author=None):
        git(
            'commit',
            '--allow-empty',
            f'--message={message}',
            *([f'--author={author}'] if author is not None else []),
            env={
                'GIT_AUTHOR_DATE': '100086400 -0100',
                'GIT_COMMITTER_DATE': '100172000 +0100',
            },
        )

        return git(
            'rev-parse',
            'HEAD',
            stdout=subprocess.PIPE,
            text=True,
        ).stdout.strip()

    recipes.mkdir(parents=True)
    git('init')
    git('config', 'user.name', 'John Doe')
    git('config', 'user.email', 'john.doe@test.com')
    git_commit('First commit.')
    commit_oldest = git_commit('Second commit.\n\nSecond line.')
    commit_middle = git_commit('Third commit.', author='Jane Doe <jane.doe@test.com>')
    commit_newest = git_commit('Fourth commit.\n\nSecond line.')

    with (recipes / '.mailmap').open('w') as fp:
        print('Jane Eyre <jane.eyre@test.com> <jane.doe@test.com>', file=fp)

    img_roots = [tmp_path / version for version in VERSIONS]
    img_unique = [
        f'arm64/hmi/apertis_{BRANCHES[0]}-arm64-hmi_{VERSIONS[0]}',
        f'amd64/sdk/apertis_{BRANCHES[1]}-amd64-sdk_{VERSIONS[1]}',
    ]
    img_shared = [
        f'arm64/fixedfunction/apertis_{branch}-arm64-fixedfunction_{version}'
        for (branch, version) in zip(BRANCHES, VERSIONS)
    ]

    for root, commit in zip(img_roots, [commit_oldest, commit_newest]):
        build_env = root / 'meta' / 'build-env-apertis-image-recipes.txt'
        build_env.parent.mkdir(parents=True)
        with build_env.open('w') as fp:
            print(f'RECIPES_COMMIT={commit}', file=fp)
            print(f'RECIPES_URL={RECIPES_URL}', file=fp)
            print('SOMETHING_ELSE=0', file=fp)

    for root, image, pkglist in zip(
        img_roots, img_shared, [TEST_PKGLIST_1, TEST_PKGLIST_2]
    ):
        path = root / image
        path.parent.mkdir(parents=True)
        with gzip.open(path.with_name(path.name + '.pkglist.gz'), 'w') as gz:
            gz.write(pkglist.encode('ascii'))
        with tarfile.open(
            path.with_name(path.name + '.changelogs.tar.gz'), 'w:gz'
        ) as tar:
            add_changelog(tar, 's1', TEST_CHANGELOG_S1)

    for root, image in zip(img_roots, img_unique):
        path = root / image
        path.parent.mkdir(parents=True)
        with gzip.open(path.with_name(path.name + '.pkglist.gz'), 'w'):
            pass
        with tarfile.open(path.with_name(path.name + '.changelogs.tar.gz'), 'w:gz'):
            pass

    changes = find_all_changes(
        ImagesRoot(img_roots[0]),
        ImagesRoot(img_roots[1]),
        RecipesRoot(recipes_root),
    )

    assert changes.images == [
        ImageChanges(
            previous=None,
            current=Path(VERSIONS[1]) / img_unique[1],
            packages={},
            source_changelogs={},
        ),
        ImageChanges(
            previous=Path(VERSIONS[0]) / img_shared[0],
            current=Path(VERSIONS[1]) / img_shared[1],
            packages={
                'p1': PackageChanges(
                    previous=PackageInfo(
                        version=Version('1'),
                        source='p1',
                        source_version=Version('1'),
                    ),
                    current=PackageInfo(
                        version=Version('2'),
                        source='s2',
                        source_version=Version('12'),
                    ),
                ),
                'p2': PackageChanges(
                    previous=PackageInfo(
                        version=Version('3'),
                        source='s1',
                        source_version=Version('3'),
                    ),
                    current=PackageInfo(
                        version=Version('5'),
                        source='s1',
                        source_version=Version('5'),
                    ),
                ),
                'p4': PackageChanges(
                    previous=PackageInfo(
                        version=Version('4'),
                        source='s1',
                        source_version=Version('4'),
                    ),
                    current=None,
                ),
                'p5': PackageChanges(
                    previous=None,
                    current=PackageInfo(
                        version=Version('0'),
                        source='p5',
                        source_version=Version('0'),
                    ),
                ),
            },
            source_changelogs={
                's1': [
                    SourceChangelogBlock(
                        author='John Doe <john.doe@test.com>',
                        timestamp='2023-08-31T06:19:46+01:00',
                        version='5',
                        changes=['s1 5'],
                    ),
                    SourceChangelogBlock(
                        author='John Doe <john.doe@test.com>',
                        timestamp='2023-06-09T19:36:15+01:00',
                        version='4',
                        changes=['s1 4'],
                    ),
                ]
            },
        ),
        ImageChanges(
            previous=Path(VERSIONS[0]) / img_unique[0],
            current=None,
            packages={},
            source_changelogs={},
        ),
    ]

    assert changes.recipes == [
        RecipesChanges(
            url=RECIPES_URL,
            previous=commit_oldest,
            current=commit_newest,
            log=[
                RecipesCommit(
                    id=commit_middle,
                    author='Jane Eyre <jane.eyre@test.com>',
                    message='Third commit.',
                    author_timestamp='1973-03-04T08:46:40-01:00',
                    commit_timestamp='1973-03-05T10:33:20+01:00',
                ),
                RecipesCommit(
                    id=commit_newest,
                    author='John Doe <john.doe@test.com>',
                    message='Fourth commit.\n\nSecond line.',
                    author_timestamp='1973-03-04T08:46:40-01:00',
                    commit_timestamp='1973-03-05T10:33:20+01:00',
                ),
            ],
        ),
    ]
