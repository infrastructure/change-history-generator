import pytest

from generate import (
    Image,
    SourceChangelogEntry,
    format_source_changelog_block,
)

from conftest import VERSIONS

TEST_CHANGELOG_BLOCK = """
 * This is a change line
 * This line is
   wrapped plus
      some weird indentation
   - 1
   - 2
 [ Jane Doe ]
 * This is Jane Doe's changelog entry.
   This does things.
   Signed-off-by: John Doe

  * Another one
 [ John Doe ]
 * Back to the original author
"""


@pytest.mark.parametrize('extension', ['.img', '.commit', ''])
@pytest.mark.parametrize(
    'prefix', ['apertis_', 'apertis_ostree_', 'isolation_rfs-', 'ospack_']
)
@pytest.mark.parametrize('version_separator', '-_')
def test_image_from_filename(
    prefix: str,
    extension: str,
    version_separator: str,
) -> None:
    full_prefix = f'{prefix}v2024pre-hmi-'

    img1 = Image.from_filename(
        f'{full_prefix}amd64-uefi{version_separator}{VERSIONS[0]}{extension}',
        version=VERSIONS[0],
    )
    assert img1 is not None
    img2 = Image.from_filename(
        f'{full_prefix}amd64-uefi{version_separator}{VERSIONS[1]}{extension}',
        version=VERSIONS[1],
    )
    assert img2 is not None
    img3 = Image.from_filename(
        f'{full_prefix}arm64-uboot{version_separator}{VERSIONS[1]}{extension}',
        version=VERSIONS[1],
    )
    assert img3 is not None

    assert img1 == img2
    assert img1 != img3
    assert img2 != img3

    assert (
        Image.from_filename(
            f'{full_prefix}arm64-uboot{version_separator}{VERSIONS[0]}{extension}',
            version=VERSIONS[1],
        )
        is None
    )


def test_format_source_changelog_block() -> None:
    assert format_source_changelog_block(
        TEST_CHANGELOG_BLOCK.splitlines(),
        version_author='John Doe',
        origin_package='test',
    ) == [
        'This is a change line',
        'This line is\nwrapped plus\nsome weird indentation\n- 1\n- 2',
        SourceChangelogEntry(
            change="This is Jane Doe's changelog entry.\nThis does things.",
            author='Jane Doe',
        ),
        SourceChangelogEntry(change='Another one', author='Jane Doe'),
        'Back to the original author',
    ]
